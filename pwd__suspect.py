from itertools import permutations 
import sys
import math

def password(pwd_len, num_of_str) :
    if pwd_len == 0:
       quit()
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    substring_list = []
    str_len = 0  

    if 1 <= pwd_len <= 25 and 0 <= num_of_str <= 10 :
       for i in range(3, num_of_str+3) :  	
          substring_list.append(sys.argv[ i ])	#Reading and appending the known substrings
       for i in substring_list :
          str_len += len(i)					#Length of all substrings
    possible_pwd_list = []
    
# If number of known passswords are zero
    if num_of_str == 0 :
       if pwd_len == 1:
          for i in alphabet:
              possible_pwd_list.append(i)
          return 26, possible_pwd_list 
       elif  1 < pwd_len <= 25:
          return 26 ** pwd_len, possible_pwd_list
          
#If password length equals to length of known sub strings
    if str_len == pwd_len :
       for i in sorted(permutations(substring_list)):
          possible_pwd_list.append(''.join(i))
       return len(possible_pwd_list), possible_pwd_list
       
#If entered strings length is less than actual password length
    elif pwd_len > str_len:
        return((26**(pwd_len - str_len))*(math.factorial(num_of_str+1))), possible_pwd_list

num_possible_pwd, possible_pwd = password(int(sys.argv[1]), int(sys.argv[2]))
print(num_possible_pwd, "suspects") #printing number of password suspects
if num_possible_pwd <= 42:
    print(possible_pwd)   #printing possible passwords if total passwords are atmost 42
